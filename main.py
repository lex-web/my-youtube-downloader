from pytube import YouTube


BASE_YOUTUBE_URL = "https://www.youtube.com"

# On demande à l'utilisateur de saisir une URL youtube et si l\'url n'est pas une URL youtube,
# on demandera à l'utilisateur de saisir à nouveau une URL youtube.
while True:
    url = input("Donner url de la video à télécharger: ")
    if url.lower().startswith(BASE_YOUTUBE_URL):
        break
    print("ERREUR: Vous devez rentrer une url Youtube!")


def on_download_progress(stream, chunk, bytes_remaining):
    """
    :param stream: L'objet de flux en cours de téléchargement
    :param chunk: Le bloc de données qui a été téléchargé
    :param bytes_remaining: Le nombre d'octets restant à télécharger

    On indique le pourcentage du fichier qui a été téléchargé
    """
    bytes_downloaded = stream.filesize - bytes_remaining
    percent = bytes_downloaded * 100 / stream.filesize

    print(f"Progression du téléchargement: {int(percent)}% ")


youtube_video = YouTube(url)


# Enregistrement d'une fonction de rappel qui sera appelée à chaque fois qu'un bloc de données est téléchargé.
youtube_video.register_on_progress_callback(on_download_progress)

print("TITRE: " + youtube_video.title)
print("NB VUES: ", youtube_video.views)

print("")
print("CHOIX DES RESOLUTIONS")
streams = youtube_video.streams.filter(only_audio=True)
index = 1
for stream in streams:
    print(f"{index} - {stream.resolution}")
    index += 1

while True:
    c = input("Choisir votre résolution: ")
    if c == "":
        print("ERREUR: Vous devez entrer un nombre!")
    else:
        try:
            c_int = int(c)
        except:
            print("ERREUR: Vous devez entrer un nombre!")
        else:
            if not 1 <= c_int <= len(streams):
                print(f"ERREUR: Vous devez entrer un nombre entre 1 et {len(streams)}")
            else:
                break

itag = streams[c_int-1].itag


# Obtenir la plus haute définition de la vidéo.
stream = youtube_video.streams.get_by_itag(itag)


print('Téléchargement...')
# On télécharge la vidéo dans le répertoire de travail actuel.
stream.download()
print("OK")