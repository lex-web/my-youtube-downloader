# My Youtube Downloader



## Getting started

Créer son environnement virtuel 
```
pip install -m venv env
source env/Scripts/activate (pour Windows)
```

## A FAIRE

- [X] Install pytube
- [X] Développement pour téchargement de la video
- [] Refactoriser le code (functions)
- [] Demande à l'utilisateur de choisir le téchargement, soit en mp4, soit en mp3
- [] Faire la partie Front (soit en utilisant un framework JS, soit avec Tkinter)
